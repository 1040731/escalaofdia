package aprivate.pt.escalaofdia;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.Spinner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_ADD_INDISPONIBILIDADE = 1;
    private final static long MILLISECONDS_PER_DAY = 1000L * 60 * 60 * 24;
    private final static long MILLISECONDS_PER_YEAR = 1000L * 60 * 60 * 24 * 365;


    private ListView listView;
    private CalendarView calendarView;
    private Date date;
    private long dateLong, dataInicial;

    private String oficial, ofSegunda, ofSexta, ofSabado, ofDomingo;

    private Calendar cal = new GregorianCalendar();

    private SQLiteDatabase baseDados, baseDadosOld, baseDadosIndisp;

    private Cursor cursor, cursorOld, cursorIndisp;

    private ArrayAdapter<String> oficialDiaAdapter;
    private ArrayList<String> oficiaisArrayList;

    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);
        calendarView = findViewById(R.id.calendarView);
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        abrirBaseDados();

        oficiaisArrayList = new ArrayList<String>();

        oficialDiaAdapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1, oficiaisArrayList);

        listView.setAdapter(oficialDiaAdapter);

        spinner = findViewById(R.id.spinner);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.oficiais, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                } else if (position == 1) {
                    limparTabela();
                } else if (position == 2) {
                    construirFds();
                    construirSemana();
                } else if (position == 13) {
                    apagarBaseDados();
                } else if (position == 14) {
                    abrirBaseDados();
                } else {
                    setOficial(spinner.getSelectedItem().toString());
                    registarOficialDia(oficial, dateLong);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                String data = dayOfMonth + "/" + (month + 1) + "/" + year;
                try {
                    DateFormat df = new SimpleDateFormat("d/M/yyyy");
                    Date date = new java.sql.Date(df.parse(data).getTime());
                    setData(date);
                    setDateLong(date.getTime());
                    spinner.setSelection(0);
                    mostrarLista(date.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                setData(date);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optionmenu_main, menu);
        return true;
    }

    private void construirFds() {
        dataInicial = dateLong;
        long dataLimite;
        dataLimite = dateLong + MILLISECONDS_PER_YEAR;
        do {
            if (isWeekend(longToDate(dateLong)) || isFeriado(longToDate(dateLong))) {

                cursor = baseDados.rawQuery("SELECT * FROM escala ORDER BY fds ASC", null);

                if (cursor != null) {
                    cursor.moveToFirst();
                    while (!(verificarDisponibilidade(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong)))) {
                        cursor.moveToNext();
                    }
                    updateOficialDia(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong));
                    cursor.close();
                }
            }
            dateLong += MILLISECONDS_PER_DAY;
            setData(longToDate(dateLong));
        } while (dateLong < dataLimite);
    }

    private void construirSemana() {
        dateLong = dataInicial;
        long dataLimite;
        dataLimite = dateLong + MILLISECONDS_PER_YEAR;
        try {
            do {
                cursor = baseDados.rawQuery("SELECT * FROM escala ORDER BY semana ASC", null);
                if (!isWeekend(longToDate(dateLong)) && !isSexta(longToDate(dateLong)) && !isSegunda(longToDate(dateLong))) {
                    if (cursor != null) {
                        cursor.moveToFirst();
                        for (int i = 0; i < cursor.getCount(); i++) {
                            while (!(verificarDisponibilidade(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong)))) {
                                cursor.moveToNext();
                            }
                            updateOficialDia(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong));
                            cursor.close();
                        }
                    }
                } else if (isSegunda(longToDate(dateLong))) {
                    if (isWeekend(longToDate(dateLong)) || isFeriado(longToDate(dateLong))) {

                    } else {
                        long diaAnterior = dateLong - (1000 * 60 * 60 * 24);
                        if (cursor != null) {
                            cursor.moveToFirst();
                            ofSegunda = cursor.getString(cursor.getColumnIndex("oficial"));
                        }
                        cursorOld = baseDadosOld.rawQuery("SELECT * FROM escala WHERE fds = " + diaAnterior + "", null);
                        if (cursorOld != null) {
                            cursorOld.moveToFirst();
                            ofDomingo = cursorOld.getString(cursorOld.getColumnIndex("oficial"));
                            cursorOld.close();
                        }
                        do {
                            while (!(verificarDisponibilidade(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong)))) {
                                cursor.moveToNext();
                                ofSegunda = cursor.getString(cursor.getColumnIndex("oficial"));
                            }
                            while (ofSegunda.equals(ofDomingo)) {
                                cursor.moveToNext();
                                ofSegunda = cursor.getString(cursor.getColumnIndex("oficial"));
                            }
                            while (!(verificarDisponibilidade(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong)))) {
                                cursor.moveToNext();
                                ofSegunda = cursor.getString(cursor.getColumnIndex("oficial"));
                            }
                        } while (ofSegunda.equals(ofDomingo));

                        updateOficialDia(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong));
                        cursor.close();
                    }
                } else if (isSexta(longToDate(dateLong))) {
                    if (isWeekend(longToDate(dateLong)) || isFeriado(longToDate(dateLong))) {

                    } else {
                        long diaSeguinte = dateLong + (1000 * 60 * 60 * 24);
                        if (cursor != null) {
                            cursor.moveToFirst();
                            ofSexta = cursor.getString(cursor.getColumnIndex("oficial"));
                            cursorOld = baseDadosOld.rawQuery("SELECT * FROM escala WHERE fds = " + diaSeguinte + "", null);
                            if (cursorOld != null) {
                                cursorOld.moveToFirst();
                                ofSabado = cursorOld.getString(cursorOld.getColumnIndex("oficial"));
                                cursorOld.close();
                            }
                            do {
                                while (!(verificarDisponibilidade(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong)))) {
                                    cursor.moveToNext();
                                    ofSexta = cursor.getString(cursor.getColumnIndex("oficial"));
                                }
                                while (ofSexta.equals(ofSabado)) {
                                    cursor.moveToNext();
                                    ofSexta = cursor.getString(cursor.getColumnIndex("oficial"));
                                }
                                while (!(verificarDisponibilidade(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong)))) {
                                    cursor.moveToNext();
                                    ofSexta = cursor.getString(cursor.getColumnIndex("oficial"));
                                }
                            } while (ofSexta.equals(ofSabado));

                            updateOficialDia(cursor.getString(cursor.getColumnIndex("oficial")), (dateLong));
                            cursor.close();
                        }
                    }
                }
                dateLong += MILLISECONDS_PER_DAY;
                setData(longToDate(dateLong));

            } while (dateLong < dataLimite);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registarOficialDia(String oficial, long data) {
        try {
            if (!isWeekend(longToDate(data)) && !isFeriado(longToDate(data))) {
                baseDados.execSQL("UPDATE escala SET semana = " + data + " WHERE oficial = '" + oficial + "'");
                baseDadosOld.execSQL("INSERT INTO escala (oficial, semana) VALUES ('" + oficial + "', '" + data + "')");
            } else {
                baseDados.execSQL("INSERT INTO escala (oficial, fds) VALUES ('" + oficial + "', '" + data + "')");
                baseDadosOld.execSQL("INSERT INTO escala (oficial, fds) VALUES ('" + oficial + "', '" + data + "')");
            }
            oficialDiaAdapter.add(oficial);

        } catch (Exception e) {
            e.printStackTrace();
        }
        oficialDiaAdapter.notifyDataSetChanged();
        mostrarLista(data);
        spinner.setSelection(0);
    }

    private boolean verificarDisponibilidade(String oficial, long data) {
        cursorIndisp = baseDadosIndisp.rawQuery("SELECT * FROM indisponibilidades WHERE oficial = '" + oficial + "' AND indInicio <= " + data + " AND indFim >= " + data + "", null);
        if (isCursorEmpty(cursorIndisp)) {
            return true;
        } else {
            return false;
        }
    }

    private void updateOficialDia(String oficial, long data) {
        try {
            if (!isWeekend(longToDate(data)) && !isFeriado(longToDate(data))) {
                baseDados.execSQL("UPDATE escala SET semana = " + data + " WHERE oficial = '" + oficial + "'");
                baseDadosOld.execSQL("INSERT INTO escala (oficial, semana) VALUES ('" + oficial + "', '" + data + "')");
            } else {
                baseDados.execSQL("UPDATE escala SET fds = " + data + " WHERE oficial = '" + oficial + "'");
                baseDadosOld.execSQL("INSERT INTO escala (oficial, fds) VALUES ('" + oficial + "', '" + data + "')");
            }
            oficialDiaAdapter.add(oficial);

        } catch (Exception e) {
            e.printStackTrace();
        }
        oficialDiaAdapter.notifyDataSetChanged();
        mostrarLista(data);
        spinner.setSelection(0);
    }

    private void limparTabela() {
        baseDados.execSQL("DELETE FROM escala");
        baseDadosOld.execSQL("DELETE FROM escala");
        baseDadosIndisp.execSQL("DELETE FROM indisponibilidades");
        Log.i("RESULTADO", " Tabelas limpas!");
        mostrarLista(dateLong);
    }

    private void mostrarLista(long dateLong) {

        cursor = baseDadosOld.rawQuery("SELECT * FROM escala WHERE semana = " + dateLong + " OR fds = " + dateLong + "", null);

        if (cursor != null) {
            oficialDiaAdapter.clear();
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Log.i("RESULTADO ", "oficial: " + cursor.getString(cursor.getColumnIndex("oficial")) + ", data: " + dateSimplifier(dateLong));
                oficialDiaAdapter.add(cursor.getString(cursor.getColumnIndex("oficial")));
                cursor.moveToNext();
            }
            cursor.close();
        }
    }

    private void abrirBaseDados() {
        try {
            baseDados = openOrCreateDatabase("appOfDia", MODE_PRIVATE, null);
            baseDados.execSQL("CREATE TABLE IF NOT EXISTS escala (oficial TEXT, semana LONG, fds LONG)");

            baseDadosOld = openOrCreateDatabase("registos", MODE_PRIVATE, null);
            baseDadosOld.execSQL("CREATE TABLE IF NOT EXISTS escala (oficial TEXT, semana LONG, fds LONG)");

            baseDadosIndisp = openOrCreateDatabase("indisponibilidades", MODE_PRIVATE, null);
            baseDadosIndisp.execSQL("CREATE TABLE IF NOT EXISTS indisponibilidades (oficial TEXT, indInicio LONG, indFim LONG)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void apagarBaseDados() {
        try {
            this.deleteDatabase("appOfDia");
            this.deleteDatabase("registos");
            this.deleteDatabase("indisponibilidades");
            Log.i("RESULTADO ", "Base de Dados apagadas!");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.OptionMenuIndisponibilidade:
                Intent indisponibilidadesIntent = new Intent(this, IndisponibilidadesActivity.class);
                startActivityForResult(indisponibilidadesIntent, REQUEST_CODE_ADD_INDISPONIBILIDADE);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String oficialIndisponivel;
        long dataIndInicio = 0, dataIndFim = 0;

        if (data != null) {
            switch (requestCode) {
                case REQUEST_CODE_ADD_INDISPONIBILIDADE:
                    oficialIndisponivel = data.getStringExtra("EXTRA_OFICIAL_INDISPONIVEL");
                    int diaInicio = data.getIntExtra("EXTRA_DIA_INICIO", 0);
                    int mesInicio = data.getIntExtra("EXTRA_MES_INICIO", 0);
                    int anoInicio = data.getIntExtra("EXTRA_ANO_INICIO", 0);
                    int diaFim = data.getIntExtra("EXTRA_DIA_FIM", 0);
                    int mesFim = data.getIntExtra("EXTRA_MES_FIM", 0);
                    int anoFim = data.getIntExtra("EXTRA_ANO_FIM", 0);

                    String dataIndisponivelInicio = diaInicio + "/" + mesInicio + "/" + anoInicio;
                    try {
                        DateFormat dIni = new SimpleDateFormat("d/M/yyyy");
                        Date dateIndispInicio = new java.sql.Date(dIni.parse(dataIndisponivelInicio).getTime());
                        dataIndInicio = dateIndispInicio.getTime();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String dataIndisponivelFim = diaFim + "/" + mesFim + "/" + anoFim;
                    try {
                        DateFormat dFim = new SimpleDateFormat("d/M/yyyy");
                        Date dateIndispFim = new java.sql.Date(dFim.parse(dataIndisponivelFim).getTime());
                        dataIndFim = dateIndispFim.getTime();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        baseDadosIndisp.execSQL("INSERT INTO indisponibilidades (oficial, indInicio, indFim) VALUES ('" + oficialIndisponivel + "', " + dataIndInicio + ", " + dataIndFim + ")");
                    } catch (Exception e) {
                        Log.i("RESULTADO: ", e.toString());
                    }
                    break;
            }
        }
    }

    public Date getData() {
        return date;
    }

    public void setData(Date date) {
        this.date = date;
    }

    public long getDateLong() {
        return dateLong;
    }

    public void setDateLong(long dateLong) {
        this.dateLong = dateLong;
    }

    public String getOficial() {
        return oficial;
    }

    public void setOficial(String oficial) {
        this.oficial = oficial;
    }

    public Boolean isWeekend(Date date) {
        cal.setTime(date);
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
                cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        }
        return false;
    }

    public Boolean isSexta(Date date) {
        long sabadoOuFeriado = date.getTime() + (1000 * 60 * 60 * 24);
        if (isFeriado(longToDate(sabadoOuFeriado)) || isWeekend(longToDate(sabadoOuFeriado))) {
            return true;
        }
        return false;
    }

    public Boolean isSegunda(Date date) {
        long domingoOuFeriado = date.getTime() - (1000 * 60 * 60 * 24);
        if (isFeriado(longToDate(domingoOuFeriado)) || isWeekend(longToDate(domingoOuFeriado))) {
            return true;
        }
        return false;
    }

    public Boolean isFeriado(Date date) {
        cal.setTime(date);
        long dateLong = date.getTime();

        final long anoNovo = 1514764800000L;
        final long sexSanta = 1522368000000L;
        final long pascoa = 1522540800000L;
        final long liberdade = 1524614400000L;
        final long trabalhador = 1525132800000L;
        final long corpoDeus = 1527724800000L;
        final long portugal = 1528588800000L;
        final long nossaSenhora = 1534291200000L;
        final long impRepub = 1538697600000L;
        final long todosSantos = 1541030400000L;
        final long restIndep = 1512086400000L;
        final long imacConc = 1512691200000L;
        final long natal = 1514160000000L;

        // Dia de Ano-Novo (01 Janeiro)
        if (dateLong == anoNovo) {
            return true;
        }

        // Sexta-Feira Santa
        else if (dateLong == sexSanta) {
            return true;
        }

        // 	Páscoa
        else if (dateLong == pascoa) {
            return true;
        }

        // Dia da Liberdade - 25 de Abril
        else if (dateLong == liberdade) {
            return true;
        }

        // Dia do Trabalhador (01 Maio)
        else if (dateLong == trabalhador) {
            return true;
        }

        // Corpo de Deus
        else if (dateLong == corpoDeus) {
            return true;
        }

        // Dia de Portugal (10 Junho)
        else if (dateLong == portugal) {
            return true;
        }

        // Assunção de Nossa Senhora (15 Agosto)
        else if (dateLong == nossaSenhora) {
            return true;
        }

        // Implantação da República (05 Outubro)
        else if (dateLong == impRepub) {
            return true;
        }

        // Dia de Todos os Santos (01 Novembro)
        else if (dateLong == todosSantos) {
            return true;
        }

        // Restauração da Independência (01 Dezembro)
        else if (dateLong == restIndep) {
            return true;
        }

        // Dia da Imaculada Conceição (08 Dezembro)
        else if (dateLong == imacConc) {
            return true;
        }

        // Natal (25 Dezembro)
        else if (dateLong == natal) {
            return true;
        } else {
            return false;
        }
    }

    public Date longToDate(long dateLong) {
        Date d = new Date(dateLong);
        return d;
    }

    public String dateSimplifier(long dateLong) {
        Date mDate = longToDate(dateLong);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
        String dateText = df2.format(mDate);
        return dateText;
    }

    public boolean isCursorEmpty(Cursor cursor) {
        if (!(cursor.moveToFirst()) || cursor.getCount() == 0) {
            return true;
        } else {
            return false;
        }
    }
}

package aprivate.pt.escalaofdia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

public class IndisponibilidadesActivity extends AppCompatActivity {

    private Spinner spinnerOficialIndisponivel, spinnerDiasInicio, spinnerMesesInicio, spinnerAnosInicio, spinnerDiasFim, spinnerMesesFim, spinnerAnosFim;
    private ArrayList<CharSequence> diasArrayList, mesesArrayList, anosArrayList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indisponibilidades);

        spinnerOficialIndisponivel = findViewById(R.id.spinnerOficialIndisponivel);
        final ArrayAdapter<CharSequence> adapterOficialIndisponivel = ArrayAdapter.createFromResource(this,
                R.array.oficiais, android.R.layout.simple_spinner_item);
        adapterOficialIndisponivel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOficialIndisponivel.setAdapter(adapterOficialIndisponivel);

        spinnerDiasInicio = findViewById(R.id.spinnerDiasInicio);
        final ArrayAdapter<CharSequence> adapterDiasInicio = ArrayAdapter.createFromResource(this,
                R.array.dias , android.R.layout.simple_spinner_item);
        adapterDiasInicio.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDiasInicio.setAdapter(adapterDiasInicio);

        spinnerMesesInicio = findViewById(R.id.spinnerMesesInicio);
        final ArrayAdapter<CharSequence> adapterMesesInicio = ArrayAdapter.createFromResource(this,
                R.array.meses , android.R.layout.simple_spinner_item);
        adapterMesesInicio.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMesesInicio.setAdapter(adapterMesesInicio);

        spinnerAnosInicio = findViewById(R.id.spinnerAnosInicio);
        final ArrayAdapter<CharSequence> adapterAnosInicio = ArrayAdapter.createFromResource(this,
                R.array.anos , android.R.layout.simple_spinner_item);
        adapterAnosInicio.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAnosInicio.setAdapter(adapterAnosInicio);

        spinnerDiasFim = findViewById(R.id.spinnerDiasFim);
        final ArrayAdapter<CharSequence> adapterDiasFim = ArrayAdapter.createFromResource(this,
                R.array.dias , android.R.layout.simple_spinner_item);
        adapterDiasFim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerDiasFim.setAdapter(adapterDiasFim);

        spinnerMesesFim = findViewById(R.id.spinnerMesesFim);
        final ArrayAdapter<CharSequence> adapterMesesFim = ArrayAdapter.createFromResource(this,
                R.array.meses , android.R.layout.simple_spinner_item);
        adapterMesesFim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMesesFim.setAdapter(adapterMesesFim);

        spinnerAnosFim = findViewById(R.id.spinnerAnosFim);
        final ArrayAdapter<CharSequence> adapterAnosFim = ArrayAdapter.createFromResource(this,
                R.array.anos , android.R.layout.simple_spinner_item);
        adapterAnosFim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAnosFim.setAdapter(adapterAnosFim);

        Button btn = findViewById(R.id.botaoAdicionar);
        btn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
                Intent intent = new Intent(IndisponibilidadesActivity.this, MainActivity.class);
                String oficialIndisponivel = spinnerOficialIndisponivel.getSelectedItem().toString();
                int diaInicio = (Integer) Integer.parseInt(spinnerDiasInicio.getSelectedItem().toString());
                int mesInicio = (Integer) Integer.parseInt(spinnerMesesFim.getSelectedItem().toString());
                int anoInicio = (Integer) Integer.parseInt(spinnerAnosInicio.getSelectedItem().toString());
                int diaFim = (Integer) Integer.parseInt(spinnerDiasFim.getSelectedItem().toString());
                int mesFim = (Integer) Integer.parseInt(spinnerMesesFim.getSelectedItem().toString());
                int anoFim = (Integer) Integer.parseInt(spinnerAnosFim.getSelectedItem().toString());
                intent.putExtra("EXTRA_OFICIAL_INDISPONIVEL", oficialIndisponivel);
                intent.putExtra("EXTRA_DIA_INICIO", diaInicio);
                intent.putExtra("EXTRA_MES_INICIO", mesInicio);
                intent.putExtra("EXTRA_ANO_INICIO", anoInicio);
                intent.putExtra("EXTRA_DIA_FIM", diaFim);
                intent.putExtra("EXTRA_MES_FIM", mesFim);
                intent.putExtra("EXTRA_ANO_FIM", anoFim);
                Log.i("RESULTADO ", "oficial: " + oficialIndisponivel + ", indisponível desde " + diaInicio + "/" +
                mesInicio + "/" + anoInicio + ", até " + diaFim + "/" + mesFim + "/" + anoFim);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
